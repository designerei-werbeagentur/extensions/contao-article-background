<?php

declare(strict_types=1);

// palettes
$GLOBALS['TL_DCA']['tl_article']['palettes']['__selector__'][] = 'bgType';
$GLOBALS['TL_DCA']['tl_article']['palettes']['__selector__'][] = 'enableOverlay';

// subpalettes
$GLOBALS['TL_DCA']['tl_article']['subpalettes']['bgType_color'] = '';
$GLOBALS['TL_DCA']['tl_article']['subpalettes']['bgType_image'] = '';
$GLOBALS['TL_DCA']['tl_article']['subpalettes']['bgType_video'] = '';
$GLOBALS['TL_DCA']['tl_article']['subpalettes']['enableOverlay'] = '';

// fields
$GLOBALS['TL_DCA']['tl_article']['fields']['bgType'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('color','image','video'),
    'reference'               => &$GLOBALS['TL_LANG']['tl_article']['bgType'],
    'eval'                    => array('tl_class'=>'clr w50', 'includeBlankOption'=>true, 'submitOnChange'=>true),
    'sql'                     => "varchar(5) default ''"
);

// fields -> color
$GLOBALS['TL_DCA']['tl_article']['fields']['bgColor'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(32) default ''"
);

// fields -> image
$GLOBALS['TL_DCA']['tl_article']['fields']['imageSRC'] = array
(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr', 'extensions'=>'jpg,jpeg,png,svg'),
    'sql'                     => "binary(16) NULL"
);

// fields -> video
$GLOBALS['TL_DCA']['tl_article']['fields']['videoSRC'] = array
(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'mandatory'=>true, 'tl_class'=>'clr', 'extensions'=>'mp4,m4v,mov,wmv,webm,ogv'),
    'sql'                     => "binary(16) NULL"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['posterSRC'] = array
(
    'exclude'                 => true,
    'inputType'               => 'fileTree',
    'eval'                    => array('filesOnly'=>true, 'fieldType'=>'radio', 'tl_class'=>'w50'),
    'sql'                     => "binary(16) NULL"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['enableOverlay'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => ['tl_class'=>'w50','submitOnChange'=>true],
    'sql'                     => "char(1) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['overlayColor'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(32) default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['overlayOpacity'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true),
    'sql'                     => "varchar(32) default ''"
);
