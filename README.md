# Contao-Article-Background

## Function & usage

This extension for Contao Open Source CMS gives you the option to add different background types to the article module. You can select between multiple background colors or choose an image or video as background.

**Note:** Templating must be defined!

## Configuration

### Adjust options

The default configuration of the options is empty. You have to define it by adjusting the `config.yml` file.

```yml
contao_article_background:
  background_colors: []
  overlay_colors: []
  overlay_opacity: []  
```
