<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('contao_article_background');

        // Keep compatibility with symfony/config < 4.2
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root('contao_article_background');
        }

        $rootNode
            ->children()
                ->arrayNode('background_colors')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('overlay_colors')
                    ->scalarPrototype()->end()
                ->end()
                ->arrayNode('overlay_opacity')
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
