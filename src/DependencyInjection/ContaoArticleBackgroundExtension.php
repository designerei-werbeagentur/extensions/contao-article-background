<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle\DependencyInjection;

use designerei\ContaoArticleBackgroundBundle\DataContainer\SetBackgroundColorOptions;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Definition;

class ContaoArticleBackgroundExtension extends Extension
{
    public function getAlias(): string
    {
        return 'contao_article_background';
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter('contao_article_background.background_colors', $config['background_colors']);
        $container->setParameter('contao_article_background.overlay_colors', $config['overlay_colors']);
        $container->setParameter('contao_article_background.overlay_opacity', $config['overlay_opacity']);
    }
}
