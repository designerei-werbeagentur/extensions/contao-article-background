<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle;

use designerei\ContaoArticleBackgroundBundle\DependencyInjection\ContaoArticleBackgroundExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoArticleBackgroundBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function getContainerExtension(): ContaoArticleBackgroundExtension
    {
        return new ContaoArticleBackgroundExtension();
    }
}
