<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle\ContaoManager;

use designerei\ContaoArticleBackgroundBundle\ContaoArticleBackgroundBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoArticleBackgroundBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
