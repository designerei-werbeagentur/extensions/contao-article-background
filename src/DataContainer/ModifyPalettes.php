<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle\DataContainer;

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\DataContainer;

class ModifyPalettes
{
    #[AsCallback(table: "tl_article", target: "config.onload")]
    public function __invoke(DataContainer $dc): void
    {
        PaletteManipulator::create()
            ->addLegend('background_legend', 'expert_legend', PaletteManipulator::POSITION_BEFORE)
            ->addField(
                'bgType',
                'background_legend',
                PaletteManipulator::POSITION_APPEND
            )
            ->applyToPalette('default', $dc->table)
        ;

        PaletteManipulator::create()
            ->addField(
                'bgColor',
                PaletteManipulator::POSITION_APPEND
            )
            ->applyToSubpalette('bgType_color', $dc->table)
        ;

        PaletteManipulator::create()
            ->addField([
                'imageSRC',
                'enableOverlay',
            ],
                PaletteManipulator::POSITION_APPEND
            )
            ->applyToSubpalette('bgType_image', $dc->table)
        ;

        PaletteManipulator::create()
            ->addField([
                'videoSRC',
                'posterSRC',
                'enableOverlay',
            ],
                PaletteManipulator::POSITION_APPEND
            )
            ->applyToSubpalette('bgType_video', $dc->table)
        ;

        PaletteManipulator::create()
            ->addField([
                'overlayColor',
                'overlayOpacity'
            ],
                PaletteManipulator::POSITION_APPEND
            )
            ->applyToSubpalette('enableOverlay', $dc->table)
        ;
    }
}
