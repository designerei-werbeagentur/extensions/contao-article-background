<?php

declare(strict_types=1);

namespace designerei\ContaoArticleBackgroundBundle\DataContainer;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;


class SetOptions
{
    private array   $backgroundColors;
    private array   $overlayColors;
    private array   $overlayOpacity;

    public function __construct(
        array  $backgroundColors,
        array  $overlayColors,
        array  $overlayOpacity
    )
    {
        $this->backgroundColors = $backgroundColors;
        $this->overlayColors    = $overlayColors;
        $this->overlayOpacity = $overlayOpacity;
    }

    #[AsCallback(table: 'tl_article', target: 'fields.bgColor.options')]
    public function setBgColorOptions(): array
    {
        return $this->backgroundColors;
    }

    #[AsCallback(table: 'tl_article', target: 'fields.overlayColor.options')]
    public function setOverlayColorOptions(): array
    {
        return $this->overlayColors;
    }

    #[AsCallback(table: 'tl_article', target: 'fields.overlayOpacity.options')]
    public function setOverlayOpacityOptions(): array
    {
        return $this->overlayOpacity;
    }
}
